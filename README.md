[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Tres en Raya
## Práctica 1

Para el desarrollo de la práctica hay que descargarse el [proyecto](https://gitlab.com/ssmmaa/curso2019-20/practica1/-/archive/v0.1/practica1-v0.1.zip). Luego se debe cambiar el nombre a: `ApellidoNombreTTT`. Todo lo relativo a las normas y fecha de entrega estará detallado en la plataforma de docencia virtual de la asignatura.

La práctica consisten en realizar los agentes necesarios para jugar al juego de las **Tres en Raya**. Para el juego se utilizarán los elementos definidos en la interface `Constantes` y las clases del paquete `util`. Para que las posiciones sean iguales para todos hay que considerar que el `(0,0)` es el centro del tablero de juego. Los agentes a desarrollar serán los siguientes:

- `AgenteTablero`: Este agente tendrá que completar las siguientes tareas:
	- Localizará agentes que estén dados de alta en el servicio de páginas amarillas con el tipo de servicio `TRES_EN_RAYA`.
	- Creará un juego para dos `AgenteJugador` que tendrá asociada su propia ventana que permitirá seguir el juego. Los movimientos que realicen los jugadores deben estar claros y seguirse con facilidad. Es decir, debe quedar claro el jugador que mueve y el intervalo entre movimiento y movimiento no debe ser demasiado elevado. El ganador debe quedar claro también.
	- La tarea que lleve el juego debe estar separada de la tarea que presente los movimientos del juego. Debe diseñarse para que se puedan jugar múltiples partidas al mismo tiempo, un mínimo de tres partidas.
	- Debe controlar los ganadores de las diferentes partidas que inicie y comunicar los ganadores a los `AgenteJugador` que se lo soliciten.

- `AgenteJugador`: Este agente debe completar las siguientes tareas:
	- Se dará de alta en el servicio de páginas amarillas con el tipo de servicio `TRES_EN_RAYA` y nombre del servicio `JUGADOR`.
	- Realizará movimientos para un juego de **Tres en Raya** cuando se le solicite. Al menos debe ser capaz de jugar tres partidas al mismo tiempo, es decir, que aún no se haya proclamado un vencedor. 
	- Solicitar a los `AgenteTablero` que conoce, porque le han propuesto jugar partidas, los ganadores de las partidas que ese `AgenteTablero` haya completado. Debe presentar esa información en una ventana asociada al `AgenteJugador`.

El diseño de los protocolos de comunicación es libre, es decir, cada uno de vosotros podrá considerar las protocolos que necesite para completar las necesidades de comunicación de los agentes. Pero los agentes deben ser **robustos**, esto es, no deben finalizar de forma inesperada por el intercambio de mensajes que se pueda producir entre ellos.

Para la documentación hay que:

 1. Crear un fichero README.md en formato markdown en el repositorio de entrega. Pueden entregarse ficheros adicionales que estarán debidamente enlazados dentro del fichero README.md. Como se ha mostrado en la documentación de los Guiones de prácticas.
 2. Deberá mostrarse de forma clara y justificada el diseño de las diferentes tareas que completarán los agentes. Las tareas no serán internas al agente, es decir, tendrán que tener en cuenta las necesidades de intercambio de información entre los agentes que puedan crearlas.
 3. Para las tareas que representan los protocolos de comunicación se incluirá su diagrama UML de secuencia con el intercambio de mensajes que incluirá la *intención* del mensaje y la clase que represente el contenido. Como se ha mostrado en el [Guión de la Sesión 3](https://gitlab.com/ssmmaa/guionsesion3/-/blob/master/README.md).
 4. Se puede utilizar cualquier herramienta de diseño de Software que el alumno considere oportuna.

La práctica es de entrega **OBLIGATORIA**, para la evaluación de la asignatura, e **INDIVIDUAL**, se controlará la copia. 

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4MTkxOTg4NjAsLTE0NTcwNzAzNDNdfQ
==
-->