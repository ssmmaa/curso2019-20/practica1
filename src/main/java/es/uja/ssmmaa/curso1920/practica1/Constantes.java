/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    public static final Random aleatorio = new Random();
    public static final long ESPERA = 4000; // 4 segundos
    public static final long TIME_OUT = 2000; // 2 segundos;
    public static final int NO_ENCONTRADO = -1;
    public static final int D100 = 100; // Representa un dado de 100
    public static final int CASILLAS = 9;
    public static final String PATH = "/";
    
    /**
     * Representa los estados de las casillas del juego de Tres en Raya para su
     * representación gráfica en el tablero de juego
     */
    public enum EstadoCasilla {
        LIBRE("libre.png"), JUGADOR_1("aspa.png"), JUGADOR_2("circulo.png");
        
        private final String fichero;

        private EstadoCasilla(String fichero) {
            this.fichero = fichero;
        }

        /**
         * Obtenemos el fichero asociado a la imágen del estado su representación
         * en el tablero
         * @return fichero de la imágen.
         */
        public String getFichero() {
            return fichero;
        }
    }
    public static final EstadoCasilla[] ESTADO = EstadoCasilla.values(); 
    
    public enum Jugador {
        JUGADOR_1, JUGADOR_2;
    }
    public static final Jugador[] JUGADORES = Jugador.values();
     
    public enum TipoServicio {
        GUI, TRES_EN_RAYA, SISTEMA; 
    }
    public static final TipoServicio[] TIPOS = TipoServicio.values();
    public enum NombreServicio {
        JUGADOR, TABLERO;
    }
    public static final NombreServicio[] SERVICIOS = NombreServicio.values();
    
    public enum Color {
        ROJO, AZUL;
    }   
}
