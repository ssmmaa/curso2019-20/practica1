/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.agentes;

import static es.uja.ssmmaa.curso1920.practica1.Constantes.JUGADORES;
import es.uja.ssmmaa.curso1920.practica1.gui.Tablero;
import jade.core.Agent;

/**
 *
 * @author pedroj
 * Esqueleto de agente para la estructura general que deben tener todos los
 * agentes
 */
public class AgentePlantilla extends Agent {
    //Variables del agente
    private Tablero tableroPrueba;
    private String[] jugadores;
    
    @Override
    protected void setup() {
       //Inicialización de las variables del agente
       jugadores = new String[JUGADORES.length];
       for( int i = 0; i < JUGADORES.length; i++ )
           jugadores[i] = this.getAID().getLocalName();
           
       tableroPrueba = new Tablero("PartidaPrueba", jugadores);
       tableroPrueba.setVisible(true);
       
       //Configuración del GUI
       
       //Registro del agente en las Páginas Amarrillas
       
       //Registro de la Ontología
       
       System.out.println("Se inicia la ejecución del agente: " + this.getName());
       //Añadir las tareas principales
    }

    @Override
    protected void takeDown() {
       //Eliminar registro del agente en las Páginas Amarillas
       
       //Liberación de recursos, incluido el GUI
       tableroPrueba.dispose();
       
       //Despedida
       System.out.println("Finaliza la ejecución del agente: " + this.getName());
    }
    
    //Métodos de trabajo del agente
    
    
    //Clases internas que representan las tareas del agente
      
}
