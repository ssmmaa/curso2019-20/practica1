/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssmmaa.curso1920.practica1.util;

import jade.core.AID;

/**
 *
 * @author pedroj
 */
public class Jugador {
    private final String nickName;
    private final AID agente;

    public Jugador(String nickName, AID agente) {
        this.nickName = nickName;
        this.agente = agente;
    }

    public String getNickName() {
        return nickName;
    }

    public AID getAgente() {
        return agente;
    }

    @Override
    public String toString() {
        return "Jugador{" + "nickName=" + nickName + ", agente=" + agente + '}';
    }
}
